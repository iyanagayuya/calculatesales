package jp.alhinc.iyanaga_yuya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) {

		//支店コード・支店名
		Map<String, String> branchmap = new HashMap<>();
		//支店コード・支店金額
		Map<String, Long> salesmap = new HashMap<>();

		BufferedReader br = null;
		try {
			// コマンドライン引数で指定したディレクトリより、定義ファイルを読み込む
			File branch = new File(args[0], "branch.lst");
			//ファイルがない場合
			if (!branch.exists()) {
				System.out.print("ファイルが存在しません");
				return;
			}
			//支店定義ファイル読み込み
			FileReader fr = new FileReader(branch);
			br = new BufferedReader(fr);

			//定義ファイル抽出
			String line;
			while ((line = br.readLine()) != null) {
				// // 行をカンマ区切りで配列に変換
				String[] data = line.split("\\,");
				// MAPにデータを格納
				//支店名map 支店コード、支店名
				branchmap.put(data[0], data[1]);
				//支店売上map 支店コード、売上金額
				salesmap.put(data[0], (long) 0);

				//支店定義ファイルのフォーマットが不正な場合
				//数字固定＆3桁
				if (!data[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				//カンマor改行を含まない文字列
				if (data.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
		//ファイルの一覧を取得
		File sorce = new File(args[0]);
		File[] rcdFiles = sorce.listFiles();

		// 売上(rcd)ファイル名前格納リスト
		ArrayList<Integer> fileCode = new ArrayList<Integer>();

		//ファイルがなかった場合
		for (int i = 0; i < rcdFiles.length; i++) {
			// ファイル名が数字8桁かつ .rcd のものを検索
			if (rcdFiles[i].isFile() && rcdFiles[i].getName().matches("^[0-9]{8}\\.rcd$")) {
				//8桁番号とrcdを分ける
				String rcdName[] = rcdFiles[i].getName().split("\\.");
				//分けたものをリストへ格納：連番エラーチェックで使う
				fileCode.add(Integer.parseInt(rcdName[0]));
				// 売上(rcd)ファイル数値格納リスト
				List<String> rcdLines = new ArrayList<>();

				//売上(rcd)ファイルの数値読み込み
				try {
					FileReader fileReader = new FileReader(rcdFiles[i]);
					BufferedReader fr = new BufferedReader(fileReader);
					br = new BufferedReader(fr);


					//5つのrcdファイルの数値(支店コード、金額)を抽出
					String sales;

					while ((sales = br.readLine()) != null) {
						//rcdファイルの数値をリストへ格納
						rcdLines.add(sales);
					}
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				} finally {
					if (br != null) {
						try {
							br.close();
						} catch (IOException e) {
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
					}
					//支店に該当がなかった場合
					if (!salesmap.containsKey(rcdLines.get(0))) {
						System.out.println(rcdFiles[i].getName() + "の支店コードが不正です");
						return;
					}
					//売上額の加算
					//入れる予定の支店番号 = rcdLines.get(0)  入れる予定の金額 = rcdLines.get(1)  既に入っている金額 = salesmap.get(rcdLines.get(0))
					long salessum = salesmap.get(rcdLines.get(0)) + Long.parseLong(rcdLines.get(1));
					//合計金額が10桁超えた場合
					//long型をString型に変換してlength()で長さを求める
					if (String.valueOf(salessum).length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					//支店売上mapに格納 支店番号 合計売上金額
					salesmap.put(rcdLines.get(0), salessum);


					//売上ファイルの中身が3行以上ある場合
					int line = rcdLines.size();
					if (line != 2) {
						System.out.println(rcdFiles[i].getName() + "のフォーマットが不正です");
						return;
					}
				}
			}
			// 連番エラーのチェック
			for (int k = 0; k < fileCode.size() - 1; k++) {
				if (fileCode.get(k + 1) - fileCode.get(k) != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			//支店別集計ファイルを作成
			BufferedWriter bw = null;
			try {
				File file = new File(args[0], "branch.out");
				FileWriter fw = new FileWriter(file);
				bw = new BufferedWriter(fw);

				//支店mapと支店売上mapの数値を書き込んでいく
				for (Entry<String, Long> s : salesmap.entrySet()) {
					//支店コード＋支店名＋支店売上
					bw.write(s.getKey() + "," + branchmap.get(s.getKey()) + "," + salesmap.get(s.getKey()) + ",");
					bw.newLine();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				try {
					//ファイルに書き込む
					bw.flush();
					//ファイルをクローズする
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}
}
